# Fraud Detection in Medical Claims

This repository contains the (prototype) model for detection of the fraud 
activity in the Medicare claims. The main purpose of the project is to demonstrate
the machine learning capabilities in the 
domain of health care.

## Introduction

The general idea is to build a model to detect of the fraud in the claims data
by analysing a set of claims from a particular provider. The problem is that 
claim level labeled data is not available, i.e. we don't have the training data 
set which would tell us if this particular claim is fraudulent or hot. However, 
the provider level labels are available in the form of publicly available 
provider exclusion data. 

## Data

We use the data from the deidentified Medicare claims and the publicaly available 
exclusions data ([LEIE Downloads](https://oig.hhs.gov/exclusions/exclusions_list.asp)).